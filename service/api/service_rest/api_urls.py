from django.urls import path
from .views import (
    api_technician,
    api_list_technicians,
    api_list_appointments,
    api_change_appointment,
    )

urlpatterns = [
    path(
        "appointments/<int:id>/",
        api_change_appointment,
        name="api_change_appointment"
    ),
    path(
        "appointments/",
        api_list_appointments,
        name="api_list_appointments"
    ),
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
    ),
    path(
        "technicians/<int:id>/",
        api_technician,
        name="api_technicians"),
    ]
