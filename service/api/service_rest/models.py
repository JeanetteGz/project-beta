from django.urls import reverse
from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.pk})


class Appointment(models.Model):
    date = models.DateField()
    customer = models.CharField(max_length=100)
    time = models.TimeField()
    vin = models.CharField(max_length=20)
    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE
    )

    reason = models.CharField(max_length=200, default="")
    vip = models.CharField(max_length=10, default="")
    status = models.CharField(max_length=100, default="INCOMPLETE")
