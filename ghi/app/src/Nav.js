import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturer</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a Manufacturer</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Create an automobile</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/vehicleModels">Models</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/vehicleModels/new">Create a vehicle model</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Add a salesperson</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/technicians">Technician</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/new">Add a technician</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Add a customer</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">Add a sale</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/salespersonhistory">Sales History</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/appointments">Service Appointments</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/new">Create a service appointment</NavLink>
              </li>
              <li className="nav-item">
              <NavLink className="dropdown-item" to="/appointments/history" role="button">Search Appointments</NavLink>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
