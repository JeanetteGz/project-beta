# CarCar

Team:

* Frances - Automobile sales, inventory api: manufacturer, automobile
* Person 2 - Service, inventory api: VehicleModel

## Design

## Service microservice

The service microservice is to keep records of the service appointments. You can create a service appointment and a technician. I created the Appointment model, Technician model, and AutomobileVO model.
 In the Appointment model you have the customer name, vip, vin, date, time, and reason for the appointment. In the service history you are abe to select cancel or finish with the buttons. In the service history form you see the status of each appointment.
 The technician model has the name and employee number. After you create a technician you assing them to a service appointment.
The AutomobileVO model stores the data from the inventory service automobile model. The poller polls that data and the assigns it to the AutomobileVO model. 

## Sales microservice

For my models, I made the AutomobileVO model and included the VIN, as specificed in the assessment instructions I also included an import_href just to add to the poller. I have also added the "sold" status in order to track the inventory for the frontend sales form. I then made a Salesperson model, Customer model, and Sales model. I integrated my models using a foreign key and the AutomobileVO. To get the data that is tied to the AutomobileVO, I used the poller and created a function called get_automobiles. With that, I was able to pull the automobile data and use the VIN that is connected to thes speficied automobile. By getting the specific automobile, I could show which specific automobile was in the sales.

Explain your models and integration with the inventory
microservice, here.
